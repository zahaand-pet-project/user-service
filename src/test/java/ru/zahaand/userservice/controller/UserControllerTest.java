package ru.zahaand.userservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.zahaand.userservice.dto.request.UserRequestDto;
import ru.zahaand.userservice.exception.handler.CustomExceptionHandler;
import ru.zahaand.userservice.service.facade.UserFacade;

import java.nio.charset.StandardCharsets;

@WebMvcTest(UserController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserControllerTest {

    @MockBean
    private UserFacade userFacade;

    private MockMvc mockMvc;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final String VALID_MOBILE_PHONE = "88001234567";
    private static final String VALID_UUID = "e75378ab-ce91-4f17-8158-75a2530d3197";
    private static final UserRequestDto VALID_USER_REQUEST_DTO = UserRequestDto.builder()
            .mobilePhone(VALID_MOBILE_PHONE)
            .build();

    private static final String BASE_ENDPOINT = "/user";
    private static final String GET_ALL_USERS_ENDPOINT = BASE_ENDPOINT;
    private static final String GET_USER_BY_MOBILE_PHONE_ENDPOINT = BASE_ENDPOINT + "/" + VALID_MOBILE_PHONE;
    private static final String CREATE_NEW_USER_ENDPOINT = BASE_ENDPOINT + "/create";
    private static final String UPDATE_USER_ENDPOINT = BASE_ENDPOINT + "/update";
    private static final String DELETE_USER_ENDPOINT = BASE_ENDPOINT + "/delete";


    @BeforeEach
    void setUp() {
        mockMvc = getMockMvc();
    }

    private MockMvc getMockMvc() {
        return MockMvcBuilders.standaloneSetup(
                        new UserController(userFacade))
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    void getAllUsers_shouldReturn_ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GET_ALL_USERS_ENDPOINT)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void getUserByMobilePhone_shouldReturn_ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GET_USER_BY_MOBILE_PHONE_ENDPOINT)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void createNewUser_shouldReturn_isCreated() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(CREATE_NEW_USER_ENDPOINT)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(VALID_USER_REQUEST_DTO)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void update_shouldReturn_ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch(UPDATE_USER_ENDPOINT)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(VALID_USER_REQUEST_DTO))
                        .param("uuid", VALID_UUID))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void deleteUserByMobilePhone_shouldReturn_ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(DELETE_USER_ENDPOINT)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("mobilePhone", VALID_MOBILE_PHONE))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}