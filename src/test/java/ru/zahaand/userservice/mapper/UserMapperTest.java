package ru.zahaand.userservice.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.zahaand.userservice.dto.request.UserRequestDto;
import ru.zahaand.userservice.dto.response.UserResponseDto;
import ru.zahaand.userservice.entity.user.Role;
import ru.zahaand.userservice.entity.user.Status;
import ru.zahaand.userservice.entity.user.User;

import java.util.UUID;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserMapperTest {

    @InjectMocks
    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    private static final String VALID_MOBILE_PHONE = "11111111111";
    private static final String VALID_UUID = "e75378ab-ce91-4f17-8158-75a2530d3197";

    @Test
    void mapUserToDto() {
        User user = getUserEntity();
        UserResponseDto expectedUserResponseDto = getUserResponseDto();

        UserResponseDto userResponseDto = userMapper.mapUserToDto(user);

        Assertions.assertEquals(expectedUserResponseDto, userResponseDto);
    }

    @Test
    void mapDtoToUser() {
        UserRequestDto userRequestDto = getUserRequestDto();
        User expectedUser = getUserEntity();

        User user = userMapper.mapDtoToUser(userRequestDto);

        Assertions.assertEquals(expectedUser.getMobilePhone(), user.getMobilePhone());
    }

    private User getUserEntity() {
        return User.builder()
                .uuid(UUID.fromString(VALID_UUID))
                .mobilePhone(VALID_MOBILE_PHONE)
                .role(Role.USER)
                .status(Status.ACTIVE)
                .build();
    }

    private UserResponseDto getUserResponseDto() {
        return UserResponseDto.builder()
                .uuid(UUID.fromString(VALID_UUID))
                .mobilePhone(VALID_MOBILE_PHONE)
                .build();
    }

    private UserRequestDto getUserRequestDto() {
        return UserRequestDto.builder()
                .mobilePhone(VALID_MOBILE_PHONE)
                .build();
    }
}
