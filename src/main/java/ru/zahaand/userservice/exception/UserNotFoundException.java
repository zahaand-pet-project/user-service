package ru.zahaand.userservice.exception;

import java.util.UUID;

public class UserNotFoundException extends RuntimeException {

    private static final String USER_NOT_FOUND_BY_UUID_MESSAGE = "User not found, uuid = %s";

    private static final String USER_NOT_FOUND_BY_MOBILE_PHONE_MESSAGE = "User not found, mobilePhone = %s";

    public UserNotFoundException(UUID uuid) {
        super(String.format(USER_NOT_FOUND_BY_UUID_MESSAGE, uuid));
    }

    public UserNotFoundException(String mobilePhone) {
        super(String.format(USER_NOT_FOUND_BY_MOBILE_PHONE_MESSAGE, mobilePhone));
    }
}
