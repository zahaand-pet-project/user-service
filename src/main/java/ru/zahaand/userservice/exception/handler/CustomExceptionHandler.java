package ru.zahaand.userservice.exception.handler;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.zahaand.userservice.dto.response.ExceptionResponseDto;
import ru.zahaand.userservice.exception.MobilePhoneAlreadyTakenException;
import ru.zahaand.userservice.exception.UserNotFoundException;

@RestControllerAdvice
@Slf4j
@Data
public class CustomExceptionHandler {

    @ExceptionHandler({
            UserNotFoundException.class,
            MobilePhoneAlreadyTakenException.class
    })
    public ResponseEntity<ExceptionResponseDto> handleException(Exception exception) {
        String message = exception.getMessage();
        log.error(message);
        log.trace(message, exception);
        HttpStatus status = HttpStatus.BAD_REQUEST;
        if (exception instanceof UserNotFoundException) {
            status = HttpStatus.NOT_FOUND;
        } else if (exception instanceof MobilePhoneAlreadyTakenException) {
            status = HttpStatus.CONFLICT;
        }
        return ResponseEntity.status(status)
                .body(new ExceptionResponseDto(message));
    }
}