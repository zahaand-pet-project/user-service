package ru.zahaand.userservice.exception;

import java.util.UUID;

public class MobilePhoneAlreadyTakenException extends RuntimeException {

    public static final String MOBILE_PHONE_ALREADY_TAKEN_MESSAGE = "Mobile phone already taken = %s, client uuid = %s";

    public MobilePhoneAlreadyTakenException(String mobilePhone, UUID uuid) {
        super(String.format(MOBILE_PHONE_ALREADY_TAKEN_MESSAGE, mobilePhone, uuid));
    }
}
