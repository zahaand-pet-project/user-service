package ru.zahaand.userservice.controller;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.zahaand.userservice.dto.request.UserRequestDto;
import ru.zahaand.userservice.dto.response.UserResponseDto;
import ru.zahaand.userservice.service.facade.UserFacade;

import java.util.List;
import java.util.UUID;

import static ru.zahaand.userservice.util.ValidationPatterns.MOBILE_PHONE_PATTERN;
import static ru.zahaand.userservice.util.ValidationPatterns.UUID_PATTERN;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserFacade userFacade;

    @GetMapping
    public ResponseEntity<List<UserResponseDto>> getAll() {
        List<UserResponseDto> allUsers = userFacade.getAll();
        return ResponseEntity.ok(allUsers);
    }

    @GetMapping("/{mobilePhone}")
    public ResponseEntity<UserResponseDto> get(
            @PathVariable @Pattern(regexp = MOBILE_PHONE_PATTERN) @NotNull String mobilePhone) {
        UserResponseDto dto = userFacade.get(mobilePhone);
        return ResponseEntity.ok(dto);
    }

    @PostMapping("/create")
    public ResponseEntity<Void> create(
            @RequestBody UserRequestDto requestDto) {
        userFacade.create(requestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .build();
    }

    @PatchMapping("/update{uuid}")
    public ResponseEntity<Void> update(
            @RequestParam("uuid") @Pattern(regexp = UUID_PATTERN) UUID uuid,
            @RequestBody UserRequestDto requestDto) {
        userFacade.update(uuid, requestDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete{mobilePhone}")
    public ResponseEntity<Void> delete(
            @RequestParam("mobilePhone") @Pattern(regexp = MOBILE_PHONE_PATTERN) String mobilePhone) {
        userFacade.delete(mobilePhone);
        return ResponseEntity.ok().build();
    }
}