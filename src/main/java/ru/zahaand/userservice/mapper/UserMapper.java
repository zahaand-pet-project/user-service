package ru.zahaand.userservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.zahaand.userservice.dto.request.UserRequestDto;
import ru.zahaand.userservice.dto.response.UserResponseDto;
import ru.zahaand.userservice.entity.user.User;

/**
 * Allow map User to dto
 *
 * @see User
 * @see UserResponseDto
 */
@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    UserResponseDto mapUserToDto(User user);

    User mapDtoToUser(UserRequestDto dto);
}