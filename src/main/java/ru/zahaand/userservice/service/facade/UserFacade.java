package ru.zahaand.userservice.service.facade;

import lombok.Data;
import org.springframework.stereotype.Component;
import ru.zahaand.userservice.dto.request.UserRequestDto;
import ru.zahaand.userservice.dto.response.UserResponseDto;
import ru.zahaand.userservice.entity.user.User;
import ru.zahaand.userservice.mapper.UserMapper;
import ru.zahaand.userservice.service.ProfileService;
import ru.zahaand.userservice.service.UserService;

import java.util.List;
import java.util.UUID;

import static ru.zahaand.userservice.mapper.UserMapper.USER_MAPPER;

/**
 * Facade for all Services
 * Maps entity to dto and dto to entity
 *
 * @see UserService
 * @see ProfileService
 * @see UserMapper
 * @see UserResponseDto
 * @see UserRequestDto
 */
@Component
@Data
public class UserFacade {

    private final UserService userService;

    private final ProfileService profileService;

    public List<UserResponseDto> getAll() {
        List<User> users = userService.getAll();
        return users.stream()
                .map(USER_MAPPER::mapUserToDto)
                .toList();
    }

    public UserResponseDto get(String mobilePhone) {
        User user = userService.get(mobilePhone);
        return USER_MAPPER.mapUserToDto(user);
    }

    public UserResponseDto get(UUID uuid) {
        User user = userService.get(uuid);
        return USER_MAPPER.mapUserToDto(user);
    }

    public void create(UserRequestDto dto) {
        User user = USER_MAPPER.mapDtoToUser(dto);
        userService.create(user);
    }

    public void update(UUID uuid, UserRequestDto requestDto) {
        userService.update(uuid, requestDto);
    }

    public void delete(String mobilePhone) {
        userService.delete(mobilePhone);
    }
}