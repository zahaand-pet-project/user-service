package ru.zahaand.userservice.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MimeTypeUtils;
import ru.zahaand.userservice.dto.request.UserRequestDto;
import ru.zahaand.userservice.entity.user.Role;
import ru.zahaand.userservice.entity.user.Status;
import ru.zahaand.userservice.entity.user.User;
import ru.zahaand.userservice.exception.MobilePhoneAlreadyTakenException;
import ru.zahaand.userservice.exception.UserNotFoundException;
import ru.zahaand.userservice.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Выполняет операции CRUD над пользователями в базе даных.
 * Все методы являются транзакционными.
 *
 * @see User
 */
@Service
@Transactional(readOnly = true)
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final StreamBridge streamBridge;

    /**
     * Получает всех пользователей из базы данных и сохраняет их в список.
     *
     * @return список всех полученных пользователей.
     */
    public List<User> getAll() {
        List<User> allUsers = userRepository.findAll();
        log.info("Get all users: SUCCESS");
        return allUsers;
    }

    /**
     * Получает пользователя из базы данных по номеру мобильного телефона.
     *
     * @param mobilePhone – номер мобильного телефона пользователя для получения из базы данных.
     * @return пользователя, полученного из базы данных по номеру мобильного телефона.
     * @throws UserNotFoundException если пользователь с указанным номером мобильного телефона отсутсвует в базе данных.
     */
    public User get(String mobilePhone) {
        User user = userRepository.findByMobilePhone(mobilePhone).orElseThrow(() -> new UserNotFoundException(mobilePhone));
        log.info("Get user by mobile phone: SUCCESS, mobile phone = {}", mobilePhone);
        return user;
    }

    /**
     * Получает пользователя из базы данных по универсальному уникальному идентификатору.
     *
     * @param uuid – универсальный уникальный идентификатор пользователя для получения из базы данных.
     * @return пользователя, полученного из базы данных по универсальному уникальному идентификатору.
     * @throws UserNotFoundException если пользователь с указанным универсальным уникальным идентификатором
     *                               отсутсвует в базе данных.
     */
    public User get(UUID uuid) {
        User user = userRepository.findByUuid(uuid).orElseThrow(() -> new UserNotFoundException(uuid));
        log.info("Get user by uuid: SUCCESS, uuid = {}", uuid);
        return user;
    }

    /**
     * Добавляет нового пользователя в базу данных.
     * Проверяет наличие любого пользователя с таким же номером мобильного телефона в базе данных.
     * Если мобильный телефон свободен:
     * Генерирует универсальный уникальный идентификатор пользователя.
     * Устанавливает роль USER.
     * Устанавливает статус ACTIVE.
     * Сохраняет пользователя в базу данных.
     * Отправляет сообщение в поток «user-out-0».
     *
     * @param user – пользователь для добавления в базу данных.
     * @throws MobilePhoneAlreadyTakenException если номер мобильного телефона уже занят.
     */
    @Transactional
    public void create(@NonNull User user) {
        String mobilePhone = user.getMobilePhone();
        checkMobilePhone(mobilePhone);
        user.setUuid(UUID.randomUUID());
        user.setRole(Role.USER);
        user.setStatus(Status.ACTIVE);
        userRepository.save(user);
        streamBridge.send("user-out-0",
                MessageBuilder.withPayload(user)
                        .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                        .build());
        log.info("Create user: SUCCESS, uuid = {}", user.getUuid());

    }

    /**
     * Обновляет номер мобильного телефона пользователя в базе данных по универсальному уникальному идентификатору.
     * Если новый номер мобильного телефона не совпадает с номером мобильного телефона пользователя,
     * проверяет наличие любого пользователя с таким же номером мобильного телефона в базе данных.
     * Если мобильный телефон свободен, обновляет номер мобильного телефона пользователя.
     *
     * @param uuid       – универсальный уникальный идентификатор пользователя для обновления в базе данных.
     * @param requestDto – объект UserRequestDto с новым номером мобильного телефона.
     * @throws UserNotFoundException            если пользователь с указанным универсальным уникальным идентификатором
     *                                          отсутсвует в базе данных.
     * @throws MobilePhoneAlreadyTakenException если новый номер мобильного телефона уже занят.
     */
    @Transactional
    public void update(UUID uuid, @NonNull UserRequestDto requestDto) {
        User user = userRepository.findByUuid(uuid).orElseThrow(() -> new UserNotFoundException(uuid));
        String mobilePhone = requestDto.getMobilePhone();
        if (!mobilePhone.equals(user.getMobilePhone())) {
            checkMobilePhone(mobilePhone);
            user.setMobilePhone(mobilePhone);
            userRepository.save(user);
            log.info("Update mobile phone: SUCCESS, uuid = {}", uuid);
        }
    }

    /**
     * Удаляет пользователя из базы данных по номеру мобильного телефона.
     *
     * @param mobilePhone – номер мобильного телефона пользователя для удаления из базы данных.
     * @throws UserNotFoundException если пользователь с указанным номером мобильного телефона отсутсвует в базе данных.
     */
    @Transactional
    public void delete(String mobilePhone) {
        Optional<User> user = userRepository.findByMobilePhone(mobilePhone);
        if (user.isPresent()) {
            userRepository.deleteByMobilePhone(mobilePhone);
            log.info("Delete user by mobile phone: SUCCESS, mobile phone = {}", mobilePhone);
        } else {
            throw new UserNotFoundException(mobilePhone);
        }
    }

    /**
     * Вспомогательный метод.
     * Проверяет, не занят ли номер мобильного телефона другим пользователем.
     *
     * @param mobilePhone – номер мобильного телефона для проверки.
     * @throws MobilePhoneAlreadyTakenException если номер мобильного телефона уже занят.
     */
    private void checkMobilePhone(String mobilePhone) {
        User user = userRepository.findByMobilePhone(mobilePhone).orElse(null);
        if (user != null) {
            throw new MobilePhoneAlreadyTakenException(mobilePhone, user.getUuid());
        }
    }
}