package ru.zahaand.userservice.repository;

import org.springframework.stereotype.Repository;
import ru.zahaand.userservice.entity.user.User;

import java.util.Optional;
import java.util.UUID;

/**
 * Repository of User
 * Has all needed simple queries
 *
 * @see User ;
 */
@Repository
public interface UserRepository extends BaseRepository<User> {

    Optional<User> findByUuid(UUID uuid);

    Optional<User> findByMobilePhone(String mobilePhone);

    boolean existsByMobilePhone(String mobilePhone);

    void deleteByMobilePhone(String mobilePhone);
}