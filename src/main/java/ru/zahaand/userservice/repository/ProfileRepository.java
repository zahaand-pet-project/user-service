package ru.zahaand.userservice.repository;

import org.springframework.stereotype.Repository;
import ru.zahaand.userservice.entity.user.Profile;

/**
 * Repository of Profile
 * Has all needed simple queries
 *
 * @see Profile ;
 */
@Repository
public interface ProfileRepository extends BaseRepository<Profile> {
}
