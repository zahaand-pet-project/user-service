package ru.zahaand.userservice.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static ru.zahaand.userservice.util.ValidationPatterns.MOBILE_PHONE_PATTERN;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDto {

    @NotBlank
    @Pattern(regexp = MOBILE_PHONE_PATTERN)
    private String mobilePhone;
}