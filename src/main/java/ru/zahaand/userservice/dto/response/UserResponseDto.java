package ru.zahaand.userservice.dto.response;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

import static ru.zahaand.userservice.util.ValidationPatterns.MOBILE_PHONE_PATTERN;
import static ru.zahaand.userservice.util.ValidationPatterns.UUID_PATTERN;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResponseDto {

    @NotBlank
    @Pattern(regexp = UUID_PATTERN)
    private UUID uuid;

    @NotBlank
    @Pattern(regexp = MOBILE_PHONE_PATTERN)
    private String mobilePhone;
}