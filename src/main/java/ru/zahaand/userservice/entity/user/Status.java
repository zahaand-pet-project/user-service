package ru.zahaand.userservice.entity.user;

import lombok.NoArgsConstructor;

/**
 * Represented as status enum
 */
@NoArgsConstructor
public enum Status {
    ACTIVE,
    NOT_ACTIVE,
    BLOCKED
}