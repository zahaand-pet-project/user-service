package ru.zahaand.userservice.entity.user;

import lombok.NoArgsConstructor;

/**
 * Represented as role enum
 */
@NoArgsConstructor
public enum Role {
    ADMIN,
    USER
}