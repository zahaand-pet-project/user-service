package ru.zahaand.userservice.entity.user;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.zahaand.userservice.entity.BaseEntity;

import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "users")
@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity {

    /**
     * Used to connect to other services
     * Represented as uuid
     * Unique, Not null
     */
    @Column(name = "uuid", nullable = false, unique = true)
    private UUID uuid;

    /**
     * Represented as 11-character string
     * Unique, Not null
     */
    @Column(name = "mobile_phone", nullable = false, unique = true)
    private String mobilePhone;

    /**
     * Represented as Role enum
     * Values: ADMIN, USER
     * Not null
     *
     * @see Role
     */
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    /**
     * Represented as Status enum
     * Values: ACTIVE, NOT_ACTIVE, BLOCKED
     * Not null
     *
     * @see Status
     */
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return uuid.equals(user.uuid) && mobilePhone.equals(user.mobilePhone) && role == user.role && status == user.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), uuid, mobilePhone, role, status);
    }
}