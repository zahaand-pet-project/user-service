package ru.zahaand.userservice.entity.user;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.zahaand.userservice.entity.BaseEntity;

import java.util.Objects;

@Entity
@Table(name = "profiles")
@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Profile extends BaseEntity {

    /**
     * Each UserProfile have a reference to a User
     * Unique, Not null
     *
     * @see User
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ToString.Exclude
    private User user;

    /**
     * Represented as 255-character string
     * Not null
     */
    @Column(nullable = false)
    private String login;

    /**
     * Represented as encrypted 255-character string
     * Not null
     */
    @Column(nullable = false)
    private String password;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Profile profile = (Profile) o;
        return user.equals(profile.user) && login.equals(profile.login) && password.equals(profile.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), user, login, password);
    }
}